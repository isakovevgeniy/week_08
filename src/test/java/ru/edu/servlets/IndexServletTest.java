package ru.edu.servlets;
import org.junit.Before;
import org.junit.Test;
import ru.edu.db.InitDataSource;
import ru.edu.db.CRUD;
import ru.edu.db.Record;
import javax.naming.NamingException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;

public class IndexServletTest {
    String webInfIndexJsp = "/WEB-INF/index.jsp";
    String records = "records";
    HttpServletRequest request = mock(HttpServletRequest.class);
    HttpServletResponse response = mock(HttpServletResponse.class);
    RequestDispatcher dispatcher = mock(RequestDispatcher.class);
    DataSource dataSourceMock = mock(DataSource.class);
    DataSource dataSource;
    CRUD crudMock;

    @Before
    public void setupJndi() throws NamingException {
        CRUD.setInstance(mock(CRUD.class));
        crudMock = CRUD.getCRUD();

        InitDataSource initDataSource = new InitDataSource(dataSourceMock);
        dataSource = initDataSource.getDataSource();
        assertEquals(dataSourceMock, dataSource);
    }

    @Test
    public void doGetTest() throws ServletException, IOException {
        IndexServlet indexServlet = new IndexServlet();
        List<Record> list = new ArrayList<>();
        when(request.getRequestDispatcher(webInfIndexJsp)).thenReturn(dispatcher);
        when(crudMock.getIndex()).thenReturn(list);
        indexServlet.doGet(request, response);
        verify(request, times(1)).setAttribute(records, list);
        verify(dispatcher, times(1)).forward(request, response);
    }
}