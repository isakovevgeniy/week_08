package ru.edu.servlets;

import org.junit.Before;
import org.junit.Test;
import ru.edu.db.InitDataSource;
import ru.edu.db.CRUD;
import ru.edu.db.Record;

import javax.naming.NamingException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;
import java.io.IOException;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

public class ViewServletTest {
    String record = "record";
    String indexErrorIdParamMissing = "/index?error=id_param_missing";
    String indexErrorIdNotFound = "/index?error=id_not_found";
    String webInfViewJsp = "/WEB-INF/view.jsp";

    HttpServletRequest request = mock(HttpServletRequest.class);
    HttpServletResponse response = mock(HttpServletResponse.class);
    RequestDispatcher dispatcher = mock(RequestDispatcher.class);
    DataSource dataSourceMock = mock(DataSource.class);
    DataSource dataSource;
    CRUD crudMock;
    ViewServlet viewServlet;

    @Before
    public void setupJndi() throws NamingException {
        CRUD.setInstance(mock(CRUD.class));
        crudMock = CRUD.getCRUD();

        viewServlet = new ViewServlet();

        InitDataSource initDataSource = new InitDataSource(dataSourceMock);
        dataSource = initDataSource.getDataSource();
        assertEquals(dataSourceMock, dataSource);
    }

    @Test
    public void doGetIdNullTest() throws ServletException, IOException {
        when(request.getParameter("id")).thenReturn(null);
        viewServlet.doGet(request, response);
        verify(response, times(1)).sendRedirect(indexErrorIdParamMissing);
    }

    @Test
    public void doGetRecordNullTest() throws ServletException, IOException {
        when(request.getParameter("id")).thenReturn("1");
        when(crudMock.getById("1")).thenReturn(null);
        viewServlet.doGet(request, response);
        verify(response, times(1)).sendRedirect(indexErrorIdNotFound);
    }

    @Test
    public void doGetRecordNotNullTest() throws ServletException, IOException {
        Record record = new Record();
        when(request.getParameter("id")).thenReturn("1");
        when(crudMock.getById("1")).thenReturn(record);
        when(request.getRequestDispatcher(webInfViewJsp)).thenReturn(dispatcher);
        viewServlet.doGet(request, response);
        verify(request, times(1)).setAttribute(this.record, record);
        verify(dispatcher, times(1)).forward(request, response);
    }
}