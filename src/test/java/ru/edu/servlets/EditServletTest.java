package ru.edu.servlets;
import org.junit.Before;
import org.junit.Test;
import ru.edu.db.InitDataSource;
import ru.edu.db.CRUD;
import ru.edu.db.Record;
import javax.naming.NamingException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

public class EditServletTest {
    String record = "record";
    String webInfEditJsp = "/WEB-INF/edit.jsp";
    String utf8 = "UTF-8";
    String viewId1 = "view?id=1";
    String editStatusOkId1 = "edit?status=ok&id=0";

    HttpServletRequest request = mock(HttpServletRequest.class);
    HttpServletResponse response = mock(HttpServletResponse.class);
    RequestDispatcher dispatcher = mock(RequestDispatcher.class);
    Map<String, String[]> map = new HashMap<>();
    Record recordMock = mock(Record.class);
    DataSource dataSourceMock = mock(DataSource.class);
    DataSource dataSource;
    CRUD crudMock;
    EditServlet editServlet;

    @Before
    public void setupJndi() throws NamingException {
        CRUD.setInstance(mock(CRUD.class));
        crudMock = CRUD.getCRUD();

        editServlet= new EditServlet();
        InitDataSource initDataSource = new InitDataSource(dataSourceMock);
        dataSource = initDataSource.getDataSource();
        assertEquals(dataSourceMock, dataSource);

        map.put("id", new String[]{"1"});
        map.put("type", new String[]{"type"});
        map.put("text", new String[]{"text"});
        map.put("price", new String[]{"1"});
        map.put("publisher", new String[]{"publisher"});
        map.put("email", new String[]{"email"});
        map.put("phone", new String[]{"phone"});
        map.put("picture_url", new String[]{"picture_url"});
    }

    @Test
    public void doGetIdNotNullTest() throws ServletException, IOException {
        Record record = new Record();

        when(request.getParameter("id")).thenReturn("1");
        when(request.getRequestDispatcher(webInfEditJsp)).thenReturn(dispatcher);
        when(crudMock.getById("1")).thenReturn(record);

        editServlet.doGet(request, response);
        verify(request, times(1)).setAttribute(this.record, record);
        verify(response, times(1)).setCharacterEncoding(utf8);
        verify(dispatcher, times(1)).forward(request, response);
    }


    @Test
    public void doGetIdIsNullTest() throws ServletException, IOException {
        when(request.getParameter("id")).thenReturn(null);
        when(request.getRequestDispatcher(webInfEditJsp)).thenReturn(dispatcher);
        editServlet.doGet(request, response);
        verify(request, times(1)).setAttribute(record, null);
        verify(response, times(1)).setCharacterEncoding(utf8);
        verify(dispatcher, times(1)).forward(request, response);
    }

    @Test
    public void doPostTitleIsNullTest() throws ServletException, IOException {
        map.put("title", new String[]{null});
        when(request.getParameterMap()).thenReturn(map);
        when(recordMock.getTitle()).thenReturn(null);
        editServlet.doPost(request, response);
        verify(response, times(1)).sendRedirect(viewId1);
    }

    @Test
    public void doPostTitleIsNotNullTest() throws ServletException, IOException {
        EditServlet editServlet = new EditServlet();
        map.put("title", new String[]{"title"});
        when(request.getParameterMap()).thenReturn(map);
        when(recordMock.getTitle()).thenReturn(null);
        editServlet.doPost(request, response);
        verify(response, times(0)).sendRedirect(editStatusOkId1);
    }
}