package ru.edu.db;

import org.junit.Test;
import java.math.BigDecimal;
import static org.junit.Assert.assertEquals;

public class RecordTest  {

    @Test
    public void recordTest() {
        Record record = new Record();

        record.setId(1L);
        record.setTitle("title");
        record.setType("type");
        record.setPrice(new BigDecimal(1));
        record.setText("text");
        record.setPublisher("publisher");
        record.setEmail("email");
        record.setPhone("phone");
        record.setPictureUrl("pictureURL");

        assertEquals(Long.valueOf(1l), record.getId());
        assertEquals("title", record.getTitle());
        assertEquals("type", record.getType());
        assertEquals(new BigDecimal(1), record.getPrice());
        assertEquals("text", record.getText());
        assertEquals("publisher", record.getPublisher());
        assertEquals("email", record.getEmail());
        assertEquals("phone", record.getPhone());
        assertEquals("pictureURL", record.getPictureUrl());
        assertEquals("text", record.getShort(10));
        assertEquals("te...", record.getShort(2));
    }
}
