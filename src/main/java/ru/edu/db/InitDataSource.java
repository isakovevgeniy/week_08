package ru.edu.db;


import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

public class InitDataSource {

    /**
     * dataSource.
     */
    private DataSource dataSource;

    /**
     * Constructor.
     *
     * @param dataSourceMock - dataSourceMock
     */
    public InitDataSource(final DataSource dataSourceMock)
            throws NamingException {

        System.setProperty(Context.INITIAL_CONTEXT_FACTORY,
                "org.osjava.sj.SimpleContextFactory");
        System.setProperty("org.osjava.sj.jndi.shared", "true");
        Context ic = new InitialContext();
        ic.rebind("java:/comp/env", ic);
        ic.rebind("java:/comp/env/jdbc/dbLink", dataSourceMock);
        ic.rebind("jdbc/dbLink", dataSourceMock);
        Context ctx = new InitialContext();
        Context env = (Context) ctx.lookup("java:/comp/env");
        dataSource = (DataSource) env.lookup("jdbc/dbLink");

    }

    /**
     * getDataSource.
     * @return - DataSource
     */
    public synchronized DataSource getDataSource() {
        return dataSource;
    }
}
