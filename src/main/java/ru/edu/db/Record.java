package ru.edu.db;

import java.math.BigDecimal;

public class Record {

    /**
     * ID.
     */
    private Long id;

    /**
     * title.
     */
    private String title;

    /**
     * Тип услуги.
     */
    private String type;

    /**
     * price.
     */
    private BigDecimal price;

    /**
     * Текст.
     */
    private String text;

    /**
     * Publisher.
     */
    private String publisher;

    /**
     * E-mail.
     */
    private String email;

    /**
     * Phone.
     */
    private String phone;

    /**
     * url картинки.
     */
    private String pictureUrl;

    /**
     * getId.
     * @return id
     */
    public Long getId() {
        return id;
    }

    /**
     * Установить ID.
     * @param pId
     */
    public void setId(final Long pId) {
        id = pId;
    }

    /**
     * getTitle.
     * @return title
     */
    public String getTitle() {
        return title;
    }


    /**
     * Установить заголовок.
     * @param pTitle
     */
    public void setTitle(final String pTitle) {
        title = pTitle;
    }


    /**
     * Получить тип.
     * @return type
     */
    public String getType() {
        return type;
    }

    /**
     * Установить тип.
     * @param pType
     */
    public void setType(final String pType) {
        type = pType;
    }

    /**
     * Получить цену.
     * @return price
     */
    public BigDecimal getPrice() {
        return price;
    }

    /**
     * Установить цену.
     * @param pPrice
     */
    public void setPrice(final BigDecimal pPrice) {
        this.price = pPrice;
    }

    /**
     * Получить текст.
     * @return text
     */
    public String getText() {
        return text;
    }

    /**
     * Установить текст.
     * @param pText
     */
    public void setText(final String pText) {
        text = pText;
    }

    /**
     * Получить автора.
     * @return publisher
     */
    public String getPublisher() {
        return publisher;
    }

    /**
     * Установить автора.
     * @param pPublisher
     */
    public void setPublisher(final String pPublisher) {
        publisher = pPublisher;
    }

    /**
     * E-mail.
     * @return email
     */
    public String getEmail() {
        return email;
    }

    /**
     * E-mail.
     * @param pEmail
     */
    public void setEmail(final String pEmail) {
        email = pEmail;
    }

    /**
     * Phone.
     * @return phone
     */
    public String getPhone() {
        return phone;
    }

    /**
     * Phone.
     * @param pPhone
     */
    public void setPhone(final String pPhone) {
        phone = pPhone;
    }

    /**
     * Img url.
     * @return pictureUrl
     */
    public String getPictureUrl() {
        return pictureUrl;
    }

    /**
     * Img url.
     * @param pPictureUrl
     */
    public void setPictureUrl(final String pPictureUrl) {
        pictureUrl = pPictureUrl;
    }

    /**
     * Краткое описание.
     * @param length
     * @return text
     */
    public String getShort(final int length) {
        if (text.length() <= length) {
            return text;
        }
        return text.substring(0, length) + "...";
    }

}

