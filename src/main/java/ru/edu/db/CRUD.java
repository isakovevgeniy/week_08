package ru.edu.db;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.sql.DataSource;
import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.Statement;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.sql.Connection;

public class CRUD {

    /**
     * Выбрать все.
     */
    public static final String SELECT_ALL_SQL =
            "SELECT * FROM records";

    /**
     * Выбрать запись по ID.
     */
    public static final String SELECT_BY_ID =
            "SELECT * FROM records WHERE id=?";

    /**
     * Добавить запись по таблицу.
     */
    public static final String INSERT_SQL =
            "INSERT INTO records (title, type, text, price, "
                    + "publisher, email, phone, picture_url) "
                    + "VALUES (?, ?, ?, ?, ?, ?, ?, ?)";

    /**
     * Обновить запись по таблице.
     */
    public static final String UPDATE_SQL =
            "UPDATE records SET title=?, type=?, text=?, price=?, "
                    + "publisher=?, email=?, phone=?, picture_url=? WHERE id=?";

    /**
     * CRUD Singleton.
     */
    private static CRUD crud;

    /**
     * Источник.
     */
    private static DataSource dataSource;

    /**
     * Конструктор.
     * @param pDatasource ...
     */
    public CRUD(final DataSource pDatasource) {
        dataSource = pDatasource;

    }

    /**
     * Getter.
     *
     * @return CRUD
     */
    public static CRUD getCRUD() {
        if (crud == null) {
            synchronized (CRUD.class) {
                if (crud == null) {
                    try {
                        Context context = new InitialContext();
                        Context env = (Context) context
                                .lookup("java:/comp/env");
                        dataSource = (DataSource) env.lookup("jdbc/dbLink");
                    } catch (Exception e) {
                        throw new RuntimeException(e.getMessage(), e);
                    }
                    crud = new CRUD(dataSource);
                }
            }
        }
        return crud;
    }

    /**
     * Получить записи.
     *
     * @return List<Record>
     */
    public List<Record> getIndex() {
        return query(SELECT_ALL_SQL);
    }

    /**
     * Запрос.
     *
     * @param sql
     * @param values
     * @return List<Record>
     */
    private List<Record> query(final String sql, final Object... values) {
        PreparedStatement statement;
        ResultSet resultSet;
        List<Record> result = new ArrayList<>();
        try {
            statement = getConnection().prepareStatement(sql);
            for (int i = 0; i < values.length; i++) {
                statement.setObject(i + 1, values[i]);
            }
            resultSet = statement.executeQuery();
            while (resultSet.next()) {
                result.add(map(resultSet));
            }
            resultSet.close();
            statement.close();
        } catch (SQLException e) {
            throw new RuntimeException("CRUD.query() SQLException caught" + e);
        }
        return result;
    }

    /**
     * Записать.
     *
     * @param resultSet
     * @return Record
     * @throws SQLException
     */
    private Record map(final ResultSet resultSet) throws SQLException {
        Record record = new Record();
        record.setId(resultSet.getLong("id"));
        record.setTitle(resultSet.getString("title"));
        record.setType(resultSet.getString("type"));
        record.setPrice(new BigDecimal(resultSet.getString("price")));
        record.setText(resultSet.getString("text"));
        record.setPublisher(resultSet.getString("publisher"));
        record.setEmail(resultSet.getString("email"));
        record.setPhone(resultSet.getString("phone"));
        record.setPictureUrl(resultSet.getString("picture_url"));
        return record;
    }

    /**
     * Получить соединение.
     *
     * @return Connection
     */
    private Connection getConnection() {
        try {
            return dataSource.getConnection();
        } catch (SQLException e) {
            throw new RuntimeException("CRUD.getConnection()"
                    + " SQLException caught", e);
        }
    }

    /**
     * Получить запись по ID.
     *
     * @param id
     * @return Record
     */
    public Record getById(final String id) {
        List<Record> records = query(SELECT_BY_ID, id);
        return records.get(0);
    }

    /**
     * Выполнить запрос.
     *
     * @param sql
     * @param values
     * @return ID
     */
    private int execute(final String sql, final Object... values) {
        try (PreparedStatement statement = getConnection()
                .prepareStatement(sql, Statement.RETURN_GENERATED_KEYS)) {
            for (int i = 0; i < values.length; i++) {
                statement.setObject(i + 1, values[i]);
            }
            return statement.executeUpdate();
        } catch (SQLException e) {
            throw new IllegalArgumentException("id not found");
        }
    }

    /**
     * Сохранить запись.
     *
     * @param record
     */
    public void save(final Record record) {
        if (record.getId() == null) {
            insert(record);
        } else {
            update(record);
        }
    }

    /**
     * Обновить запись.
     *
     * @param record
     */
    public void update(final Record record) {
        execute(UPDATE_SQL, record.getTitle(), record.getType(),
                record.getText(), (record.getPrice() == null)
                        ? new BigDecimal(0)
                        : record.getPrice().toPlainString(),
                record.getPublisher(),
                record.getEmail(), record.getPhone(), record.getPictureUrl(),
                record.getId());
    }

    /**
     * Добавить запись.
     *
     * @param record
     */
    public void insert(final Record record) {
        execute(INSERT_SQL, record.getTitle(), record.getType(),
                record.getText(), (record.getPrice() == null)
                        ? new BigDecimal(0) : record.getPrice().toPlainString(),
                record.getPublisher(), record.getEmail(), record.getPhone(),
                record.getPictureUrl());
    }

    /**
     * test.
     * @param testCrud ...
     */
    public static void setInstance(final CRUD testCrud) {
        crud = testCrud;
    }

}

