<!DOCTYPE html>
<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<html>
<head>
    <link rel="stylesheet" href="styles.css" type="text/css">
    <jsp:include page="/WEB-INF/templates/header.jsp">
        <jsp:param name="title" value="Login"/>
    </jsp:include>
</head>
<body>
<div class="page">
    <form method="post">
      <div class="content edit-form">
        <div class="form-item">
          <input id="login" name="login" placeholder="login"/>
        </div>
        <div class="form-item">
          <input id="password" name="password" placeholder="password" type="password"/>
        </div>
      </div>
      <div class="button-container">
          <button class="send-button">Войти</button>
      </div>
    </form>
    <%@ include file="/WEB-INF/templates/footer.jsp" %>
</div>
</body>
</html>